# News App

## How to use

First clone the project

```shell
git clone https://gitlab.com/tarek.helwani22/innoscripta-ag-frontend-test/
```

Then you should install the packages:

```shell
npm install
```

run the project in development mode:

```shell
npm run dev
```

To run the project using docker:
```shell
docker compose up --build
```

## I used a basic design in the project so I can take care of implementing the requirements more carefully
