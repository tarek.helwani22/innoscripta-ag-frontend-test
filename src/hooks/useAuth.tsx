import { useEffect, useState } from "react";
import storage from "../utiils/storage";
import { User } from "../types";
import { useMutation, useQuery } from "react-query";
import { getUserInfo } from "../api/authApi";
import { AppPaths } from "../consts/app/paths";

import { logout } from "../consts/api/logout";

type AuthContextType = {
    user?: User;
    hasToken?: boolean;
    onLogout: () => void;
    isLoadingToken: boolean;
};

export const useAuth = (): AuthContextType => {
    const [ user, setUser ] = useState<User | undefined>(undefined);
    const [ hasToken, setHasToken ] = useState<boolean>(false);
    const [ isLoadingToken, setIsLoadingToken ] = useState<boolean>(true);

    const { mutateAsync } = useMutation(() => logout());

    useEffect(() => {
        const token = storage.getToken();
        if (token) {
            setHasToken(true);
            setIsLoadingToken(false);
            getUserInfo()
                .then((response) => {
                    setUser(response.data.user);
                })
        } else {
            setIsLoadingToken(false);
        }
    }, []);

    const onLogout = () => {
        mutateAsync().then(() => {
            storage.clearToken();
            setHasToken(false);
            setUser(undefined);
            window.location.assign(AppPaths.LOGIN);
        })
    }

    return { user, hasToken, onLogout, isLoadingToken };
};
