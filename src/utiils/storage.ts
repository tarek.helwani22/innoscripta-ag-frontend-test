class Storage<T extends string | null> {
    private readonly tokenKey: string;

    constructor(tokenKey: string) {
        this.tokenKey = tokenKey;
    }

    setItem(key: string, value: T) {
        if (value === null) {
            localStorage.removeItem(key);
        } else {
            localStorage.setItem(key, JSON.stringify(value));
        }
    }


    getItem(key: string): T {
        const item = localStorage.getItem(key);
        return item ? (JSON.parse(item) as T) : (null as T); // Type assertion
    }

    removeItem(key: string) {
        localStorage.removeItem(key);
    }

    setToken(token: string) {
        this.setItem(this.tokenKey, token as T);
    }

    getToken(): T {
        return this.getItem(this.tokenKey);
    }

    clearToken() {
        this.removeItem(this.tokenKey);
    }
}

const tokenKey = "TOKEN";
const storage = new Storage<string | null>(tokenKey);

export default storage;
