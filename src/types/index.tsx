export interface Category {
    identifier: string;
    name: string;
}

export interface Source {
    identifier: string;
    name: string;
}

export interface User {
    id: number;
    name: string;
    email: string;
    sources: Source[]
    category: Category;
}

