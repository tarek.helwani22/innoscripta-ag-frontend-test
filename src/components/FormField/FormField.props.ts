import { Control, FieldErrors, FieldValues, Path } from "react-hook-form";

export type FormFieldProps<T extends FieldValues> = {
    className?: string
    name: Path<T>;
    control: Control<T>;
    label: string;
    type?: string
    rules?: Record<string, unknown>;
    errors: FieldErrors<T>;
};