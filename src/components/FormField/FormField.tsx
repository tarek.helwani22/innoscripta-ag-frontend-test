import React from "react";
import { TextField } from "@mui/material";
import { Controller, FieldValues } from "react-hook-form";
import { FormFieldProps } from "./FormField.props";

const FormField = <T extends FieldValues>({
                                              className,
                                              name,
                                              control,
                                              type,
                                              label,
                                              rules,
                                              errors,
                                          }: FormFieldProps<T>) => (
    <Controller
        name={ name }
        control={ control }
        // @ts-ignore
        defaultValue=""
        rules={ rules }
        render={ ({ field }) => (
            <TextField
                { ...field }
                className={ className }
                label={ label }
                type={ type }
                variant="outlined"
                required
                fullWidth
                error={ !!errors[name] }
                // @ts-ignore
                helperText={ errors[name]?.message }
            />
        ) }
    />
);

export default FormField;
