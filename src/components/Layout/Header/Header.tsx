import React from "react";
import { Link } from "react-router-dom";
import {
    AppBar,
    Toolbar,
    IconButton,
    Typography,
    Button,
    makeStyles,
} from "@mui/material";
import { inspect } from "util";
import styles from "./Header.module.scss";
import { AppPaths } from "../../../consts/app/paths";
import { useAuth } from "../../../hooks/useAuth";

const Header: React.FC = () => {
    const { onLogout } = useAuth();

    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" className={styles.title}>
                    News App
                </Typography>
                <Button color="inherit" component={Link} to={AppPaths.NEWS_FEED}>
                    Home
                </Button>
                <Button color="inherit" component={Link} to={AppPaths.SETTINGS}>
                    Settings
                </Button>
                <Button color="inherit" onClick={onLogout}>
                    Logout
                </Button>
            </Toolbar>
        </AppBar>
    );
};

export default Header;
