import React from "react";
import { AppLayoutProps } from "./AppLayout.props";
import { Header } from "./Header";

const AppLayout: React.FC<AppLayoutProps> = ({ children }) => {
    return (
        <div>
            <Header />
            {children}
        </div>
    );
};

export default AppLayout;
