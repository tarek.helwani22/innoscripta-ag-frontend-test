import React from 'react';
import "./App.css"
import { Toaster } from "react-hot-toast";
import { QueryClient, QueryClientProvider } from "react-query";
import {
    BrowserRouter as Router,
} from "react-router-dom";
import { AppRoutes } from "./routes";

const queryClient = new QueryClient({});

function App() {
    return (
        <div className="App">
            <QueryClientProvider client={ queryClient }>
                <Router>
                    <AppRoutes/>
                </Router>
                <Toaster position="top-right"/>
            </QueryClientProvider>
        </div>
    );
}

export default App;
