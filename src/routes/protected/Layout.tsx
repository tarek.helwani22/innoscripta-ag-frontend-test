import React from "react";
import { Navigate, Outlet } from "react-router-dom";

import { AppLayout } from "../../components/Layout";
import { useAuth } from "../../hooks/useAuth";
import { CircularProgress } from "@mui/material";

export const ProtectedRoutesLayout: React.FC = () => {
    const { hasToken, isLoadingToken } = useAuth();


    if (isLoadingToken) {
        return (
            <div style={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100vh" }}>
                <CircularProgress />
            </div>
        );
    }

    if (!hasToken) {
        return <Navigate to="/login" />;
    }

    return (
        <>
            <AppLayout>
                <Outlet/>
            </AppLayout>
        </>
    );
};
