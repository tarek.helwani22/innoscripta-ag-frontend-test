import { Navigate, RouteObject } from "react-router-dom";

import { ProtectedRoutesLayout } from "./Layout";
import NewsPage from "../../pages/News/NewsPage";
import { AppPaths } from "../../consts/app/paths";
import SettingsPage from "../../pages/Settings/SettingsPage";

export const protectedRoutes: RouteObject[] = [
    {
        path: "/",
        element: <ProtectedRoutesLayout />,
        children: [
            {
                path: AppPaths.NEWS_FEED,
                element: <NewsPage />,
            },
            {
                path: AppPaths.SETTINGS,
                element: <SettingsPage/>,
            },
            { path: "*", element: <Navigate to="." /> },
        ],
    },
];
