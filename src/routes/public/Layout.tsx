import React from "react";
import { Outlet } from "react-router-dom";

export const PublicRoutesLayout: React.FC = () => {
    return (
        <>
            <Outlet/>
        </>
    );
};
