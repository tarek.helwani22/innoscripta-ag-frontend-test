import { Navigate, RouteObject } from "react-router-dom";
import { RegisterPage } from "../../pages/Register";
import { PublicRoutesLayout } from "./Layout";
import { AppPaths } from "../../consts/app/paths";
import { LoginPage } from "../../pages/Login";

export const publicRoutes: RouteObject[] = [
    {
        element: <PublicRoutesLayout />,
        children: [
            {
                path: AppPaths.REGISTER,
                element: <RegisterPage />,
            },
            {
                path: AppPaths.LOGIN,
                element: <LoginPage />,
            },
            { path: "*", element: <Navigate to="/login" /> },
        ],
    },
];
