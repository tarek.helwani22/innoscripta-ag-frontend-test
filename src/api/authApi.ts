import { ApiEndpoints } from "../consts/api/endpoints";
import { User } from "../types";
import axios from "./apiConfig";

export interface GetUserInfoResponse {
    data: {
        user: User;
    }
}

export const getUserInfo = async (): Promise<GetUserInfoResponse> => {
    return await axios.get(ApiEndpoints.getUserInfo);
};