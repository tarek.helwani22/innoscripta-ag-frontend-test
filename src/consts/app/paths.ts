export const AppPaths = Object.freeze({
    REGISTER: "/register",
    LOGIN: "/login",
    NEWS_FEED: "/",
    PERSONALIZED: "/personalized",
    SETTINGS: "/account-settings",
});
