export const ApiEndpoints = Object.freeze({
    registerUser: "/register",
    loginUser: "/login",
    logoutUser: "/logout",
    getUserInfo: "/me",
    getNews: "/news",
    getCategories: "/categories",
    getSources: "/sources",
    updatePreferences: "/update-preferences",
});
