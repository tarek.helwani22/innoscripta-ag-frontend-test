import axios from "../../api/apiConfig";
import { ApiEndpoints } from "./endpoints";

export const logout = () => {
    return axios.post(ApiEndpoints.logoutUser);
};