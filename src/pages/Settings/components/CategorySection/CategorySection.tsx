import React from "react";
import { Radio, FormControlLabel } from "@mui/material";
import styles from "./CategorySection.module.scss";
import { CategorySectionProps } from "./CategorySection.props";
import { Category } from "../../../../types";

const CategorySection: React.FC<CategorySectionProps> = ({
                                                             categories,
                                                             selectedCategory,
                                                             handleCategoryChange,
                                                         }) => {
    return (
        <div className={styles.section}>
            <h2>Select Preferred Category:</h2>
            {categories.map((category: Category) => (
                <FormControlLabel
                    key={category.identifier}
                    control={
                        <Radio
                            checked={selectedCategory === category.identifier}
                            onChange={handleCategoryChange}
                            value={category.identifier}
                            name="category"
                        />
                    }
                    label={category.name}
                />
            ))}
        </div>
    );
};

export default CategorySection;


