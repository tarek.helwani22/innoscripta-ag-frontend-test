import { Category } from "../../../../types";
import React from "react";

export interface CategorySectionProps {
    categories: Category[];
    selectedCategory: string;
    handleCategoryChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}