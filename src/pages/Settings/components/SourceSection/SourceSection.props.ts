import { Source } from "../../../../types";
import React from "react";

export interface SourcesSectionProps {
    sources: Source[];
    selectedSources: string[];
    handleSourceChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}