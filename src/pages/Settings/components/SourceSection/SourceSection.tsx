import React from "react";
import { Checkbox, FormControlLabel } from "@mui/material";
import styles from "./SourceSection.module.scss";
import { SourcesSectionProps } from "./SourceSection.props";
import { Source } from "../../../../types";

const SourcesSection: React.FC<SourcesSectionProps> = ({
                                                           sources,
                                                           selectedSources,
                                                           handleSourceChange,
                                                       }) => {
    return (
        <div className={styles.section}>
            <h2>Select Preferred Sources:</h2>
            {sources.map((source: Source) => (
                <FormControlLabel
                    key={source.identifier}
                    control={
                        <Checkbox
                            checked={selectedSources.includes(source.identifier)}
                            onChange={handleSourceChange}
                            name={source.identifier}
                        />
                    }
                    label={source.name}
                />
            ))}
        </div>
    );
};

export default SourcesSection;