import React, { useEffect, useState } from "react";
import { SettingsPageProps } from "./SettingsPage.props";

import { Radio, FormControlLabel, Button, Checkbox, CircularProgress } from "@mui/material";
import styles from "./SettingsPage.module.scss";
import { useCategories } from "../../hooks/useCategories";
import { useSources } from "../../hooks/useSources";
import { Category, Source } from "../../types";
import { useAuth } from "../../hooks/useAuth";
import { useSettingsPage } from "./hooks/useSettingsPage";
import { CategorySection } from "./components/CategorySection";
import SourcesSection from "./components/SourceSection/SourceSection";

const SettingsPage: React.FC<SettingsPageProps> = () => {
    const { categories, isLoading: categoriesLoading } = useCategories();
    const { sources, isLoading: sourcesLoading } = useSources();

    const {
        handleCategoryChange,
        handleSourceChange,
        handleSavePreferences,
        selectedCategory,
        selectedSources
    } = useSettingsPage()

    if (categoriesLoading || sourcesLoading) {
        return <div className={ styles.loader }>
            <CircularProgress color="primary"/>
        </div>
    }

    return (
        <div className={ styles.container }>
            <CategorySection
                categories={ categories }
                selectedCategory={ selectedCategory }
                handleCategoryChange={ handleCategoryChange }
            />
            <SourcesSection
                sources={ sources }
                selectedSources={ selectedSources }
                handleSourceChange={ handleSourceChange }
            />
            <Button
                variant="contained"
                color="primary"
                onClick={ handleSavePreferences }
                className={ styles["save-button"] }
            >
                Save Preferences
            </Button>
        </div>
    );
};

export default SettingsPage;
