import { ApiEndpoints } from "../../../consts/api/endpoints";
import { UpdatePreferencesDto } from "../types";
import axios from "../../../api/apiConfig";

export const updatePreferences = (data: UpdatePreferencesDto) => {
    return axios.put(ApiEndpoints.updatePreferences, data);
};