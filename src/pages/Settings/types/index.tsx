export type UpdatePreferencesDto = {
    category?: string;
    sources?: string[];
};