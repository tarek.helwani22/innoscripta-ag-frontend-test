import React, { useEffect, useState } from "react";
import { useMutation } from "react-query";
import { UpdatePreferencesDto } from "../types";
import { updatePreferences } from "../api/updatePreferencesApi";
import toast from "react-hot-toast";
import { useAuth } from "../../../hooks/useAuth";

export const useSettingsPage = () => {
    const { mutateAsync } = useMutation((data: UpdatePreferencesDto) => updatePreferences(data));
    const { user } = useAuth();

    const [selectedCategory, setSelectedCategory] = useState<string>("");
    const [selectedSources, setSelectedSources] = useState<string[]>([]);

    useEffect(() => {
        if (user) {
            setSelectedCategory(user?.category?.identifier)
            setSelectedSources(user?.sources.map(source => source.identifier) ?? [])
        }
    }, [user])

    const handleCategoryChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSelectedCategory(event.target.value);
    };

    const handleSourceChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const sourceIdentifier = event.target.name;
        setSelectedSources((prevSelectedSources) =>
            event.target.checked
                ? [...prevSelectedSources, sourceIdentifier]
                : prevSelectedSources.filter((src) => src !== sourceIdentifier)
        );
    };

    const handleUpdatePreferences = async (data: UpdatePreferencesDto) => {
        await mutateAsync(data, {
            onSuccess: (response) => {
                toast.success("Preferences updated successfully")
            },
        });
    };

    const handleSavePreferences = async () => {
        await handleUpdatePreferences({
            category: selectedCategory,
            sources: selectedSources,
        });
    };

    return {
        selectedCategory,
        selectedSources,
        handleCategoryChange,
        handleSourceChange,
        handleSavePreferences,
    };
};
