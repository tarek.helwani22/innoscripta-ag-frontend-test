import { ApiEndpoints } from "../../../consts/api/endpoints";
import { User } from "../../../types";
import { LoginDto } from "../types";
import axios from "../../../api/apiConfig";

export interface LoginResponse {
    data: {
        user: User;
        token: string;
    }
}

export const login = (data: LoginDto): Promise<LoginResponse> => {
    return axios.post(ApiEndpoints.loginUser, data);
};