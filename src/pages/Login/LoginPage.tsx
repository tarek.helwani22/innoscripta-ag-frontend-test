import React from "react";
import { Link } from "react-router-dom";
import {
    Button,
    Container,
    TextField,
    Typography,
    Card,
    CssBaseline,
    Box,
} from "@mui/material";
import { LoginPageProps } from "./LoginPage.props";
import styles from "./LoginPage.module.scss";
import { AppPaths } from "../../consts/app/paths";
import { useForm } from "react-hook-form";
import { useLoginPage } from "./hooks/useLoginPage";
import { LoginDto } from "./types";
import FormField from "../../components/FormField/FormField";

const LoginPage: React.FC<LoginPageProps> = () => {
    const { handleLogin } = useLoginPage();
    const {
        handleSubmit,
        control,
        formState: { errors },
    } = useForm<LoginDto>();

    return (
        <Box className={ styles.container }>
            <CssBaseline/>
            <Card
                className={ styles.card }
                elevation={ 3 }
            >
                <Typography
                    paddingY={ 3 }
                    variant="h5"
                    component="h2"
                >
                    Login
                </Typography>
                <form onSubmit={ handleSubmit(handleLogin) }>
                    <FormField
                        className={styles.formField}
                        name="email"
                        control={control}
                        label="Email"
                        rules={{
                            required: "Email is required",
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                message: "Invalid email address",
                            },
                        }}
                        errors={errors}
                    />
                    <FormField
                        className={styles.formField}
                        name="password"
                        control={control}
                        label="Password"
                        type={"password"}
                        rules={{
                            required: "Password is required",
                            minLength: {
                                value: 8,
                                message: "Password must be at least 8 characters",
                            },
                        }}
                        errors={errors}
                    />
                    <Button
                        className={ styles.loginButton }
                        type="submit"
                        variant="contained"
                        fullWidth
                        color="primary"
                    >
                        Log In
                    </Button>
                </form>
                <Typography
                    variant="body2"
                    className={ styles.registerLink }
                >
                    Don't have an account?{ " " }
                    <Link
                        to={ AppPaths.REGISTER }
                        className={ styles.link }
                    >
                        Register here
                    </Link>
                </Typography>
            </Card>
        </Box>
    );
};

export default LoginPage;
