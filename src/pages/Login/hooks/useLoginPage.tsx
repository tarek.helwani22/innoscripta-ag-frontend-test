import React, { useState } from "react";
import { useMutation } from "react-query";
import storage from "../../../utiils/storage";
import { LoginDto } from "../types";
import { login } from "../api/loginApi";
import { useNavigate } from "react-router-dom";
import { AppPaths } from "../../../consts/app/paths";

export const useLoginPage = () => {
    const { mutateAsync } = useMutation((data: LoginDto) => login(data));
    const navigate = useNavigate();

    const handleLogin = async (data: LoginDto) => {
        try {
            const response = await mutateAsync(data);
            storage.setToken(response.data.token);

            navigate(AppPaths.NEWS_FEED)
        } catch (err) {

        }
    };

    return {
        handleLogin,
    };
};
