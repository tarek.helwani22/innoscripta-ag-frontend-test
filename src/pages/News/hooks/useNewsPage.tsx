import { useQuery, useMutation } from "react-query";
import { getNews } from "../api/newsApi";
import React, { useEffect, useState } from "react";
import { NewsItem } from "../types";
import { SelectChangeEvent } from "@mui/material";
import toast from "react-hot-toast";
import { useAuth } from "../../../hooks/useAuth";

interface NewsFilters {
    page: number;
    category: string;
    sources: string[];
    fromDate?: string;
    keyword: string;
}

export const useNewsPage = () => {
    const [filters, setFilters] = useState<NewsFilters>({
        page: 1,
        category: "",
        sources: [],
        fromDate: undefined,
        keyword: "",
    });

    const [news, setNews] = useState<NewsItem[]>([]);
    const [shouldRefetch, setShouldRefetch] = useState<boolean>(false);
    const [isLoadingMoreNews, setIsLoadingMoreNews] = useState<boolean>(false);

    const { user } = useAuth();

    const { data, isLoading, refetch } = useQuery(
        ["news", filters],
        () => getNews({
            keyword: filters.keyword,
            category: filters.category,
            sources: filters.sources,
            fromDate: filters.fromDate ? new Date(filters.fromDate) : undefined,
            page: filters.page
        }),
        {
            enabled: shouldRefetch,
        },
    );

    useEffect(() => {
        toast.success("Filters will be updated to your preferences", {
            duration: 3000,
        });
    }, []);

    useEffect(() => {
        // Make sure that we are fetching the first page with the new filters
        if (shouldRefetch && filters.page === 1) {
            setNews(data?.data ?? []);
            setShouldRefetch(false);
        }
    }, [shouldRefetch, filters.page])

    useEffect(() => {
        const fetchMoreNews = async () => {
            if (isLoadingMoreNews || isLoading) return;

            // First patch of data already fetched
            if (filters.page === 1 && data) {
                setNews(data?.data);
                return;
            }

            setIsLoadingMoreNews(true);

            try {
                const { data: moreNews } = await refetch(); // Fetch new data

                if (moreNews) {
                    setNews((prevNews) => [ ...prevNews, ...moreNews.data ])
                }
            } catch (error: any) {
                toast.error("Error fetching more news: ", error?.message);
            } finally {
                setIsLoadingMoreNews(false);
            }
        };
        fetchMoreNews();
    }, [filters.page, isLoading]);

    useEffect(() => {
        if (user) {
            setFilters((prevFilters) => ({
                ...prevFilters,
                category: user?.category?.identifier ?? "",
                sources: user?.sources.map((source) => source.identifier) ?? []
            }));
            setShouldRefetch(true);
        }
    }, [user]);

    // Handle the filters

    const handleFilterChange = (field: string, value: any) => {
        setFilters((prevFilters) => ({
            ...prevFilters,
            [field]: value,
        }));
    };

    const handleCategoryChange = (event: SelectChangeEvent) => {
        handleFilterChange("category", event.target.value);
    };

    const handleSourceChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const source = event.target.name;
        handleFilterChange(
            "sources",
            event.target.checked
                ? [...filters.sources, source]
                : filters.sources.filter((s) => s !== source)
        );
    };

    const handleFromDateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        handleFilterChange("fromDate", event.target.value);
    };

    const handleKeywordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        handleFilterChange("keyword", event.target.value);
    };


    const handleFilterClick = () => {
        handleFilterChange("page", 1);
        setShouldRefetch(true);
    };


    return {
        news,
        setNews,
        isFetching: isLoading || isLoadingMoreNews,
        filters,
        setFilters,
        handleFilterClick,
        handleCategoryChange,
        handleSourceChange,
        handleFromDateChange,
        handleKeywordChange
    };
};
