import { Category, Source } from "../../../../types";
import React from "react";
import { SelectChangeEvent } from "@mui/material";

export interface NewsFilterProps {
    categories: Category[];
    sources: Source[];
    selectedCategory: string;
    selectedSources: string[];
    fromDate: string | undefined;
    keyword: string;
    onCategoryChange: (event: SelectChangeEvent) => void;
    onSourceChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    onFromDateChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    onKeywordChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    onFilterClick: () => void;
}