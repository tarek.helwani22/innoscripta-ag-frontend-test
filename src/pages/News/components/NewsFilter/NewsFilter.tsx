import React from "react";
import {
    Typography,
    FormControl,
    Select,
    MenuItem,
    TextField,
    InputAdornment,
    Button,
    Checkbox,
    FormControlLabel,
} from "@mui/material";
import styles from "./NewsFilter.module.scss"
import { NewsFilterProps } from "./NewsFilter.props";
import SearchIcon from "@mui/icons-material/Search";

const NewsFilter: React.FC<NewsFilterProps> = ({
                                                   categories,
                                                   sources,
                                                   selectedCategory,
                                                   selectedSources,
                                                   fromDate,
                                                   keyword,
                                                   onCategoryChange,
                                                   onSourceChange,
                                                   onFromDateChange,
                                                   onKeywordChange,
                                                   onFilterClick,
                                               }) => {
    return (
        <div className={ styles.filters }>
            <TextField
                className={ styles.searchField }
                label="Search articles by keyword"
                variant="outlined"
                fullWidth
                value={ keyword }
                onChange={ onKeywordChange }
                InputProps={ {
                    endAdornment: (
                        <InputAdornment position="end">
                            <SearchIcon color="primary"/>
                        </InputAdornment>
                    ),
                } }
            />
            <FormControl className={ styles.filterControl }>
                <Typography
                    variant="subtitle1"
                    gutterBottom
                >
                    Categories
                </Typography>
                <Select
                    value={ selectedCategory }
                    onChange={ onCategoryChange }
                    displayEmpty
                    inputProps={ { "aria-label": "Categories" } }
                >
                    <MenuItem value="">
                        <em>All</em>
                    </MenuItem>
                    { categories.map((category) => (
                        <MenuItem
                            key={ category.identifier }
                            value={ category.identifier }
                        >
                            { category.name }
                        </MenuItem>
                    )) }
                </Select>
            </FormControl>
            <FormControl
                component="fieldset"
                className={ styles.filterControl }
            >
                <Typography
                    variant="subtitle1"
                    gutterBottom
                >
                    Sources
                </Typography>
                { sources.map((source) => (
                    <FormControlLabel
                        key={ source.identifier }
                        control={
                            <Checkbox
                                checked={ selectedSources.includes(source.identifier) }
                                onChange={ onSourceChange }
                                name={ source.identifier }
                                color="primary"
                            />
                        }
                        label={ source.name }
                    />
                )) }
            </FormControl>
            <TextField
                className={ styles.filterControl }
                label="From Date"
                variant="outlined"
                type="date"
                fullWidth
                value={ fromDate }
                onChange={ onFromDateChange }
                InputLabelProps={ {
                    shrink: true,
                } }
            />
            <Button
                className={ styles.filterButton }
                variant="contained"
                color="primary"
                onClick={ onFilterClick }
            >
                Filter
            </Button>
        </div>
    );
};

export default NewsFilter;