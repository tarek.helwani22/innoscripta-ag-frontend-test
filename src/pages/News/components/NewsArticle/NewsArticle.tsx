import React from "react";
import styles from "./NewsArticle.module.scss";
import { Typography } from "@mui/material";
import { NewsArticleProps } from "./NewsArticle.props";

const NewsArticle: React.FC<NewsArticleProps> = ({ article, isLastItem, lastNewsElementRef }) => {
    return (
        <div ref={isLastItem ? lastNewsElementRef : undefined} className={styles.article}>
            <Typography variant="h6" component="h3" gutterBottom>
                {article.title}
            </Typography>
            {article.description && (
                <Typography variant="body2" color="textSecondary">
                    {article.description}
                </Typography>
            )}
            {article.author && (
                <Typography variant="body2" color="textSecondary">
                    Author: {article.author}
                </Typography>
            )}
            <a href={article.url} target="_blank" rel="noopener noreferrer">
                Read more
            </a>
            <Typography variant="body2" color="textSecondary">
                Source: {article.source}
            </Typography>
            <Typography variant="body2" color="textSecondary">
                Category: {article.category}
            </Typography>
            <Typography variant="body2" color="textSecondary">
                Published At: {article.publishedAt}
            </Typography>
        </div>
    );
};

export default NewsArticle;