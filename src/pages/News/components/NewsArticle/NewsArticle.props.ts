import { NewsItem } from "../../types";
import React from "react";

export interface NewsArticleProps {
    article: NewsItem;
    isLastItem: boolean;
    lastNewsElementRef: React.Ref<HTMLDivElement> | null;
}