import React from "react";
import { NewsItem } from "../../types";

export interface NewsListProps {
    newsList: NewsItem[];
    lastNewsElementRef: React.Ref<HTMLDivElement> | null;
    isFetching: boolean
}