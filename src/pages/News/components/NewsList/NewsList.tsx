import React from "react";
import { CircularProgress } from "@mui/material";
import styles from "./NewsList.module.scss";
import { NewsListProps } from "./NewsList.props";
import { NewsArticle } from "../NewsArticle";

const NewsList: React.FC<NewsListProps> = ({ newsList, lastNewsElementRef, isFetching }) => {
    return (
        <div className={ styles.newsFeed }>
            { newsList.map((article, index) => (
                <NewsArticle
                    key={index}
                    article={ article }
                    isLastItem={ index === newsList.length - 1 }
                    lastNewsElementRef={ lastNewsElementRef }
                />
            )) }
            { isFetching && (
                <div className={ styles.loaderContainer }>
                    <CircularProgress color="primary"/>
                </div>
            ) }
        </div>
    );
};

export default NewsList;
