import { NewsItem } from "../types";
import axios from "../../../api/apiConfig";
import { ApiEndpoints } from "../../../consts/api/endpoints";

interface GetNewsParams {
    keyword?: string;
    category?: string;
    sources?: string[];
    fromDate?: Date
    page?: number
}

export interface GetNewsResponse {
    data: NewsItem[];
}

export const getNews = async (
    params?: GetNewsParams
): Promise<GetNewsResponse> => {
    return await axios.get(ApiEndpoints.getNews, {
        params
    });
};