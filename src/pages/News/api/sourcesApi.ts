import { ApiEndpoints } from "../../../consts/api/endpoints";
import axios from "../../../api/apiConfig";
import { Source } from "../../../types";

export interface GetSourcesResponse {
    data: {
        sources: Source[];
    };
}

export const getSources = async (): Promise<GetSourcesResponse> => {
    return await axios.get(ApiEndpoints.getSources);
};
