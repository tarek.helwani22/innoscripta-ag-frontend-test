import { ApiEndpoints } from "../../../consts/api/endpoints";
import axios from "../../../api/apiConfig";
import { Category } from "../../../types";

export interface GetCategoriesResponse {
    data: {
        categories: Category[];
    };
}

export const getCategories = async (): Promise<GetCategoriesResponse> => {
    return await axios.get(ApiEndpoints.getCategories);
};
