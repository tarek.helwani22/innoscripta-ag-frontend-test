import React, { useCallback, useEffect, useRef, useState } from "react";
import { Container, Card, CssBaseline, Typography, SelectChangeEvent } from "@mui/material";
import { NewsPageProps } from "./NewsPage.props";
import styles from "./NewsPage.module.scss";
import { useNewsPage } from "./hooks/useNewsPage";
import { useCategories } from "../../hooks/useCategories";
import { useSources } from "../../hooks/useSources";
import { NewsList } from "./components/NewsList";
import { NewsFilter } from "./components/NewsFilter";


const NewsPage: React.FC<NewsPageProps> = () => {
    const { categories } = useCategories();
    const { sources } = useSources();

    const {
        news,
        isFetching,
        filters,
        setFilters,
        handleFilterClick,
        handleCategoryChange,
        handleSourceChange,
        handleKeywordChange,
        handleFromDateChange
    } = useNewsPage();

    const observer = useRef<IntersectionObserver | null>(null);

    const lastNewsElementRef = useCallback(
        (node: HTMLDivElement | null) => {
            if (isFetching) return;
            if (observer.current) observer.current.disconnect();
            observer.current = new IntersectionObserver((entries) => {
                if (entries[0].isIntersecting) {
                    setFilters((prevFilters) => ({
                        ...prevFilters,
                        page: prevFilters.page + 1
                    }));
                }
            });
            if (node) observer.current.observe(node);
        },
        [ isFetching ]
    );

    return (
        <Container className={ styles.container }>
            <CssBaseline/>
            <Card
                className={ styles.card }
                elevation={ 3 }
            >
                <Typography
                    variant="h5"
                    component="h2"
                    paddingBottom={ 2 }
                >
                    News Feed
                </Typography>
                <NewsFilter
                    categories={ categories }
                    sources={ sources }
                    selectedCategory={ filters.category }
                    selectedSources={ filters.sources }
                    fromDate={ filters.fromDate }
                    keyword={ filters.keyword }
                    onCategoryChange={ handleCategoryChange }
                    onSourceChange={ handleSourceChange }
                    onFromDateChange={ handleFromDateChange }
                    onKeywordChange={ handleKeywordChange }
                    onFilterClick={ handleFilterClick }
                />
                <NewsList
                    newsList={ news }
                    lastNewsElementRef={ lastNewsElementRef }
                    isFetching={ isFetching }
                />
            </Card>
        </Container>
    );
};

export default NewsPage;
