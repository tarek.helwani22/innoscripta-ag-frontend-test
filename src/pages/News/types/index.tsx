export interface NewsItem {
    title: string;
    description: string;
    author: string;
    url: string;
    source: string;
    category: string;
    publishedAt: string;
}

