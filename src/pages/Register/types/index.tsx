export type RegisterDto = {
    name: string;
    email: string;
    password: string;
    password_confirmation: string;
};

export type RegisterFormData = {
    name: string;
    email: string;
    password: string;
    passwordConfirmation: string;
};