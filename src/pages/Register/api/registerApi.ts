import { ApiEndpoints } from "../../../consts/api/endpoints";
import { RegisterDto } from "../types";
import { User } from "../../../types";
import axios from "../../../api/apiConfig";

export interface RegisterResponse {
    data: {
        user: User;
        token: string;
    }
}

export const register = (data: RegisterDto): Promise<RegisterResponse> => {
    return axios.post(ApiEndpoints.registerUser, data);
};