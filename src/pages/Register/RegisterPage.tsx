import React from "react";
import {
    Button,
    Typography,
    Card,
    CssBaseline,
    Box,
} from "@mui/material";
import { RegisterPageProps } from "./RegisterPage.props";
import styles from "./RegisterPage.module.scss";
import { useRegisterPage } from "./hooks/useRegisterPage";
import { useForm } from "react-hook-form";
import FormField from "../../components/FormField/FormField";
import { RegisterDto, RegisterFormData } from "./types";

const RegisterPage: React.FC<RegisterPageProps> = () => {
    const { handleRegister } = useRegisterPage();
    const {
        handleSubmit,
        control,
        formState: { errors },
    } = useForm<RegisterFormData>();

    return (
        <Box className={ styles.container }>
            <CssBaseline/>
            <Card
                className={ styles.card }
                elevation={ 3 }
            >
                <Typography
                    paddingY={ 3 }
                    variant="h5"
                    component="h2"
                >
                    Register
                </Typography>
                <form onSubmit={ handleSubmit(handleRegister) }>
                    <FormField
                        className={ styles.formField }
                        name="name"
                        control={ control }
                        label="Name"
                        rules={ {
                            required: "Name is required",
                        } }
                        errors={ errors }
                    />
                    <FormField
                        className={ styles.formField }
                        name="email"
                        control={ control }
                        label="Email"
                        rules={ {
                            required: "Email is required",
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                message: "Invalid email address",
                            },
                        } }
                        errors={ errors }
                    />
                    <FormField
                        className={ styles.formField }
                        name="password"
                        control={ control }
                        label="Password"
                        type={ "password" }
                        rules={ {
                            required: "Password is required",
                            minLength: {
                                value: 8,
                                message: "Password must be at least 8 characters",
                            },
                        } }
                        errors={ errors }
                    />
                    <FormField
                        className={ styles.formField }
                        name="passwordConfirmation"
                        control={ control }
                        label="Confirm Password"
                        type={ "password" }
                        rules={ {
                            required: "Confirm Password is required",
                            validate: (value: string) =>
                                value === control._formValues["password"] ||
                                "Passwords do not match",
                        } }
                        errors={ errors }
                    />
                    <Button
                        className={ styles.registerButton }
                        type="submit"
                        variant="contained"
                        fullWidth
                        color="primary"
                    >
                        Register
                    </Button>
                </form>
            </Card>
        </Box>
    );
};

export default RegisterPage;