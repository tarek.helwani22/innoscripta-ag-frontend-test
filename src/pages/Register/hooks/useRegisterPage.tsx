import { RegisterDto, RegisterFormData } from "../types";
import { register } from "../api/registerApi";
import { useMutation } from "react-query";
import storage from "../../../utiils/storage";
import { useNavigate } from "react-router-dom";
import { AppPaths } from "../../../consts/app/paths";
import { useAuth } from "../../../hooks/useAuth";

export const useRegisterPage = () => {
    const { mutateAsync } = useMutation((data: RegisterDto) => register(data));
    const navigate = useNavigate();

    const handleRegister = async (data: RegisterFormData) => {
        try {
            const dataWithConfirmation: RegisterDto = {
                name: data.name,
                email: data.email,
                password: data.password,
                password_confirmation: data.passwordConfirmation,
            };

            const response = await mutateAsync(dataWithConfirmation);
            storage.setToken(response.data.token);

            navigate(AppPaths.NEWS_FEED)
        } catch (err) {

        }
    };

    return {
        handleRegister,
    };
};
